<?php
require_once "vendor/autoload.php";
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
    try {
        $db = new PDO("sqlite:bot.db");
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     }  catch (Exception $e) {
        echo "Unable to connect";
        echo $e->getMessage();
        exit;
    }
    //Guzzle 1er site
    //connection au site de hand ball de chambery
        $client = new Client();
        $response = $client->request('GET','https://www.chamberysavoiehandball.com/actualites/');
        $res = (string)$response->getBody();
    //echo $res;
    //DOMCrawler 1er site
    //recuperation image de hand ball chambery
        $crawler = new Crawler($res);
        $reponse2 = $crawler->filterXpath('//img')->extract(array('src'));
    //recuperation du texte de hand ball chambery
        $reponse = $crawler->filterXpath('//span[contains(@class,"txt")] |//span[contains(@class,"date")] 
            | //span[contains(@class,"title")] ')->extract(['_text']);
    //Guzzle 2eme site
    //connection au site de la dynamo
        $client = new Client();
        $guzzle = $client->request('GET','http://ladynamo.chambery.fr/2295-numerique-innovation.htm');
        $res = (string)$guzzle->getBody();
    //DOMCrawler 2eme site
    //recuperation des image du site de la dynamo
        $crawler = new Crawler($res);
        $reponse2 = $crawler->filterXpath('//img')->extract(array('src'));
    //recuperation du textes du site de la dynamo
        $reponse = $crawler->filterXpath('//p')->extract(['_text']);
        $reponse3 = $crawler->filterXpath('//body/div')->extract(['_text']);
        $req = $db->prepare('INSERT INTO evenement(texte, texte_body) VALUES(:texte, :texte_body)');
        $req->execute(array(
            'texte'=> $reponse,
            'texte_body'=> $reponse3
        ));

    echo 'Le jeu a bien été ajouté !';
?>